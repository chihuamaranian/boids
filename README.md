# Boids

## Background

This simple 2D boids implementation was built as a combined learning experience: Learn how to use Godot, and learn how to implement boids.

I say this to underscore I am no authority on either!

I have chosen to use C# instead of GDScript. This is a personal choice. I'm familiar with C# already, and appreciate strongly typed systems.

## Process

The godot side of things was heavily influenced by the following tutorial:

https://www.youtube.com/watch?v=oFnIlNW_p10

I have removed the walls (and related boid rays) to simplify.

On the code side, I was not very sold on the code structure in the tutorial.

On top of wanting to convert it to C#, I also wanted it to more closely follow the first-principals of boid behavior outlined by Craig Reynolds:

https://www.red3d.com/cwr/boids/

## Implementation

I wanted to keep the boids library fairly decoupled from Godot. To do this, my boids library operates over any class implementing an interface IBoids.

I was unable to completely remove godot coupling though: I've used its implementation of the Vector2d struct. In practice, this should be fairly trivial to replace with any other vector implementation.

The main 'meat' of the boids implementation is in ./src/Boid/BoidExtensions

These operate over the IBoid interfaces. I was able to keep all methods here static and stateless.

Finally, we need to attach the boid code to godot nodes. This is done in BoidGodot.cs. 

Godot collision detection hooks for area2d items provide the array of neighboring boids.