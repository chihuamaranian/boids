using Boids.src.LongHorse.GodotAdapters;
using Godot;
using LongHorse.BoidsLib2D;
using System;
using System.Collections.Generic;
using Vector2 = System.Numerics.Vector2;
public class BoidGodot : Area2D, IBoid
{
    public float SeparationWeight { get; } = 0.1f;
    public float AlignmentWeight { get; } = 0.1f;
    public float CohesionWeight { get; } = 0.003f;
    public float _wallAvoidWeight { get; } = 100.0f;
    public List<IBoid> Neighbors { get; } = new List<IBoid>();
    public List<IBoid> Predators { get; } = new List<IBoid>();
    public float MaxSpeed { get; set; } = 5.0f;
    public float MinSpeed { get; set; } = 2.0f;
    public Vector2 Velocity { get; set; } = Vector2.One;
    public bool IsPredator { get; set; } = false;

    //hide the native godot Area2D GlobalPosition behind this item.
    //the reason being we are using the .net vector2 implementation instead of the godot implementation
    public new Vector2 GlobalPosition { get { return base.GlobalPosition.ToDotNet(); } set { base.GlobalPosition = value.ToGodot(); } }

    public Random _random = new Random();
    public float maxCollisionExtents;
    public Vector2 _screensize;



    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _screensize = GetViewportRect().Size.ToDotNet();
        Velocity = new Vector2((float)_random.NextDouble() * (_random.Next(2) % 2 == 0 ? 1: -1), (float)_random.NextDouble() * (_random.Next(2) % 2 == 0 ? 1 : -1));
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        this.Advance(_screensize);
    }

    public void _on_vision_area_entered(Area2D area)
    {
        if (area != this && area.IsInGroup("boid"))
        {
            var boid = (IBoid)area;
            if (boid.IsPredator)
            {
                Predators.Add(boid);
            }
            else 
            {
                Neighbors.Add(boid);
            }
            
        }
    }

    public void _on_vision_area_exited(Area2D area)
    {
        if (area != this && area.IsInGroup("boid"))
        {
            var boid = (IBoid)area;
            if (boid.IsPredator)
            {
                Predators.Remove(boid);
            }
            else
            {
                Neighbors.Remove(boid);
            }
            
        }
    }
}