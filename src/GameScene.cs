using Godot;
using LongHorse.BoidsLib2D;
using System;

public class GameScene : Node2D
{
    private int _boidCount = 250;
    private int _margin = 100;
    private Vector2 _screensize = Vector2.Zero;
    private Random _random = new Random();

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _screensize = new Vector2(800, 600);
        GD.Print("Processing GameScene._Ready()");
        GD.Print($"screen size {_screensize}");
        GD.Print($"Screen ({_screensize.x}x{_screensize.y})");

        for (var i = 0; i < _boidCount; i++)
        {
            SpawnBoid(_random, i);
        }
    }

    private void SpawnBoid(Random random, int i) 
    {
        var boid = (Area2D)ResourceLoader.Load<PackedScene>("res://src/Boid.tscn").Instance();
        GetTree().Root.GetNode("GameScene").GetNode("BoidFolder").AddChild(boid);

        if (i % 25 == 0)
        {
            ((IBoid)boid).IsPredator = true;
            ((IBoid)boid).MaxSpeed = 2.5f;
            ((IBoid)boid).MinSpeed = 1.0f;
            boid.Modulate = new Color(1,1,1,1);
        }
        else 
        {
            ((IBoid)boid).IsPredator = false;
            boid.Modulate = new Color(0,0,0,1);
        }
        
        boid.GlobalPosition = new Vector2(random.Next(_margin, (int)(_screensize.x) - _margin), random.Next(_margin, (int)(_screensize.y) - _margin));
        boid.Rotate((float)random.NextDouble());
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
    }
}
