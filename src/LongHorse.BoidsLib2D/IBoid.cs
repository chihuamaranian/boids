﻿using System.Collections.Generic;
using System.Numerics;

namespace LongHorse.BoidsLib2D
{
    public interface IBoid
    {
        float SeparationWeight { get; }
        float AlignmentWeight { get; }
        float CohesionWeight { get; }
        float _wallAvoidWeight { get; }
        float MaxSpeed { get; set; }
        float MinSpeed { get; set; }
        List<IBoid> Neighbors { get; }
        List<IBoid> Predators { get; }
        Vector2 Velocity { get; set; }
        Vector2 GlobalPosition { get; set; }
        float GlobalRotation { get; set; }
        bool IsPredator { get; set; }
    }
}
