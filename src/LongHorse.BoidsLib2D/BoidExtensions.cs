﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace LongHorse.BoidsLib2D
{

    public static class BoidExtensions
    {
        public static float AngleBetween(Vector2 a, Vector2 b)
        {
            return (float)Math.Atan2((b.Y - a.Y), b.X - a.X);
        }

        public static void Advance(this IBoid boid, Vector2 screenSize)
        {
            var flock = boid.Flock(boid.Neighbors, boid.CohesionWeight) * (boid.IsPredator ? 10.0f : 1.0f);
            var align = boid.IsPredator ? Vector2.Zero : boid.Align(boid.Neighbors, boid.AlignmentWeight);
            var avoid = boid.Avoid(boid.Neighbors, boid.SeparationWeight);
            var avoidPredators = boid.Avoid(boid.Predators, boid.SeparationWeight * 1000);

            boid.Velocity = boid.Velocity + flock + align + avoid + avoidPredators;
            if (boid.Velocity.LengthSquared() > boid.MaxSpeed * boid.MaxSpeed)
            {
                boid.Velocity = Vector2.Normalize(boid.Velocity) * boid.MaxSpeed;
            }
            if (boid.Velocity.LengthSquared() < boid.MinSpeed * boid.MinSpeed)
            {
                boid.Velocity = Vector2.Normalize(boid.Velocity) * boid.MinSpeed;
            }

            boid.GlobalPosition += boid.Velocity;
            boid.GlobalRotation = AngleBetween(boid.GlobalPosition, boid.GlobalPosition + boid.Velocity);

            boid.WrapAround(screenSize);
        }
        private static Vector2 Flock(this IBoid boid, IEnumerable<IBoid> neighbors, float weight)
        {
            var neighborCount = neighbors.Count();
            if (neighborCount == 0) return Vector2.Zero;

            float meanX = 0.0f;
            float meanY = 0.0f;
            foreach (var n in neighbors)
            {
                meanX += n.GlobalPosition.X;
                meanY += n.GlobalPosition.Y;
            }
            meanX /= neighborCount;
            meanY /= neighborCount;

            var deltaX = meanX - boid.GlobalPosition.X;
            var deltaY = meanY - boid.GlobalPosition.Y;

            return new Vector2(deltaX, deltaY) * weight;
        }

        private static Vector2 Align(this IBoid boid, IEnumerable<IBoid> neighbors, float weight)
        {
            var neighborCount = neighbors.Count();
            if (neighborCount == 0) return Vector2.Zero;

            float meanXVel = 0.0f;
            float meanYVel = 0.0f;
            foreach (var n in neighbors)
            {
                meanXVel += n.Velocity.X;
                meanYVel += n.Velocity.Y;
            }
            meanXVel /= neighborCount;
            meanYVel /= neighborCount;

            var deltaX = meanXVel - boid.Velocity.X;
            var deltaY = meanYVel - boid.Velocity.Y;

            return new Vector2(deltaX, deltaY) * weight;
        }

        private static Vector2 Avoid(this IBoid boid, IEnumerable<IBoid> neighbors, float weight)
        {
            var separationVector = Vector2.Zero;
            foreach (var n in neighbors)
            {
                var distanceSquared = Vector2.DistanceSquared(boid.GlobalPosition, n.GlobalPosition);
                separationVector += (boid.GlobalPosition - n.GlobalPosition) * (1 / (distanceSquared + float.Epsilon));
            }

            return separationVector * weight;
        }

        private static void WrapAround(this IBoid boid, Vector2 screenSize)
        {
            if (boid.GlobalPosition.X < 0) boid.GlobalPosition = new Vector2(screenSize.X, boid.GlobalPosition.Y);
            if (boid.GlobalPosition.X > screenSize.X) boid.GlobalPosition = new Vector2(0, boid.GlobalPosition.Y);
            if (boid.GlobalPosition.Y < 0) boid.GlobalPosition = new Vector2(boid.GlobalPosition.X, screenSize.Y);
            if (boid.GlobalPosition.Y > screenSize.Y) boid.GlobalPosition = new Vector2(boid.GlobalPosition.X, 0);
        }

    }
}