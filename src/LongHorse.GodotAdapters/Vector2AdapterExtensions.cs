﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boids.src.LongHorse.GodotAdapters
{
    public static class Vector2AdapterExtensions
    {
        public static Godot.Vector2 ToGodot(this System.Numerics.Vector2 v) 
        {
            return new Godot.Vector2(v.X, v.Y);
        }

        public static System.Numerics.Vector2 ToDotNet(this Godot.Vector2 v)
        {
            return new System.Numerics.Vector2(v.x, v.y);
        }

        public static IEnumerable<Godot.Vector2> ToGodot(this IEnumerable<System.Numerics.Vector2> v) 
        {
            return v.Select(s => s.ToGodot());
        }

        public static IEnumerable<System.Numerics.Vector2> ToDotNet(this IEnumerable<Godot.Vector2> v)
        {
            return v.Select(s => s.ToDotNet());
        }
    }
}
